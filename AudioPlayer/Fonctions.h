#ifndef MANIP_CSTRINGS
#define MANIP_CSTRINGS

#include <string>

void CStringToChar(char* _c, CString _cs);
bool RecupNomEtChemin(CString* _csNom, CString* _csChemin, CString _csChaineSrc);
int NbAleatoire(unsigned int max);

#endif